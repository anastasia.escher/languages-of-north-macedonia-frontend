import {defineStore} from 'pinia'
import {tsv} from 'd3'
import {Sentence, Text, Word} from 'stores/models'
import {parseTextData} from 'stores/helpers'

const TEXTS_URL =
    'https://docs.google.com/spreadsheets/d/e/2PACX-1vQOU2UfNeotUw9BbpQpFYSq2ngjjD0fSjJGNdrAQuCUCC76_HIDlIT6OaXVVHxDBfSIIcl0vYKHz1sh/pub?output=tsv'
export const useMacStore = defineStore('mac', {
    state: () => ({
        texts_mac: [] as Text[],
        sentences_mac: [] as Sentence[],
        words_mac: [] as Word[],
        meta_mac: [] as any[],
    }),
    persist: true,
    actions: {
        async getTexts() {
            try {
                const res = []
                const texts = await tsv(TEXTS_URL)

                this.meta_mac = texts
                for (const text of texts) {
                    if (text.url && text.id && text.desc) {
                        const text_data = await tsv(text.url)

                        res.push(parseTextData(text_data, text.id, text.desc))
                    }
                }
                const _texts: Text[] = []
                let _sentences: Sentence[] = []
                let _words: Word[] = []
                for (const item of res) {
                    _texts.push(item[0])
                    _sentences = _sentences.concat(item[1])
                    _words = _words.concat(item[2])
                }
                this.texts_mac = _texts
                this.sentences_mac = _sentences
                this.words_mac = _words
            } catch (err) {
                console.error(err)
            }
        },
    },
})
